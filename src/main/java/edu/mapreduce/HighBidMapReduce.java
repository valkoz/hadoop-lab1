package edu.mapreduce;

import edu.mapreduce.counters.AnyErrorCounter;
import edu.mapreduce.counters.BidNoFormatted;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

/**
 * Главный запускаемый класс
 */
public class HighBidMapReduce {

    public static void main(String[] args) throws Exception {
        // Создание конфигурации приложения
        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator", ",");
        // Настройка map reduce job
        Job job = Job.getInstance(conf, "max word");
        // Задание main класса
        job.setJarByClass(HighBidMapReduce.class);
        // Задание класса для mapper
        job.setMapperClass(HighBidMapper.class);
        // Задание класса для reducer
        job.setReducerClass(HighBidReducer.class);
        // Задание класса для combiner
        job.setCombinerClass(HighBidReducer.class);
        // Настройка вывода
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        // Использование SequenceFile для вывода
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        // Задание входного и выходного файла
        FileInputFormat.addInputPath(job, new Path(args[1]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        //Запуск и логирование времени
        long startTime = System.nanoTime();
        boolean success = job.waitForCompletion(true);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        if (!success) {
            // Ошибка на этапе исполнения
            System.out.println("Job exited with error!!!");
        } else {
            // Исполнение прошло успешно
            System.out.println("Success!!! Duration: " + (duration / 1000000) + "ms");
            // Получить занчения счетчиков
            Counter anyError = job.getCounters().findCounter(AnyErrorCounter.ANY_ERROR);
            Counter bid_no_formatted
                    = job.getCounters().findCounter(BidNoFormatted.BID_NO_FORMATTED_ERROR);
            // Залогировать значения счетчиков
            System.out.println("Any error=[" + anyError.getValue() + "]");
            System.out.println("Integer parse error=[" + bid_no_formatted.getValue() + "]");
        }
    }
}
