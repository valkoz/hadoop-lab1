package edu.mapreduce;

import com.google.gson.Gson;
import edu.mapreduce.counters.AnyErrorCounter;
import edu.mapreduce.counters.BidNoFormatted;
import edu.mapreduce.dto.BidDto;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Тесты запускаются при билде
 */
public class HighBidMapReduceTest {

    private HighBidMapper mapper;
    private HighBidReducer reducer;
    private MapDriver<Object, Text, Text, Text> mapDriver;
    private ReduceDriver<Text, Text, Text, Text> reduceDriver;

    @Before
    public void setup() {
        mapper = new HighBidMapper();
        reducer = new HighBidReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }


    /**
     * Тест mapper с корректными данными
     *
     * @throws IOException
     */
    @Test
    public void mapperGoodTest() throws IOException {
        // Формирование строки
        String strExample = "bcbc973f1a93e22de83133f360759f04\t20131019134022114\t1\tCALAIF9UcIi\tMozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)\t59.34.170.*\t216\t237\t3\t7ed515fe566938ee6cfbb6ebb7ea4995\tea4e49e1a4b0edabd72386ee533de32f\tnull\tALLINONE_F_Width2\t1000\t90\tNa\tNa\t50\t7336\t294\t50\tnull\t2259\t10059,14273,10117,10075,10083,10102,10006,10148,11423,10110,10031,10126,13403,10063\n";
        Text text1 = new Text(strExample);
        // Задание входных параметров mapper
        mapDriver.withInput(new IntWritable(1), text1);
        // Рассчет ожидаемого результата
        Gson gson = new Gson();
        BidDto dto = new BidDto("237", 294, 1);
        String json = gson.toJson(dto);
        // Задание ожидаемого результата
        mapDriver.withOutput(new Pair<Text, Text>(new Text("237"), new Text(json)));
        // Запуск теста
        mapDriver.runTest();
        // проверка счетчиков
        Counter invalidIpCounter = mapDriver.getCounters().findCounter(AnyErrorCounter.ANY_ERROR);
        Counter invalidSeparatorCounter = mapDriver.getCounters().findCounter(BidNoFormatted.BID_NO_FORMATTED_ERROR);
        Assert.assertEquals(0, invalidIpCounter.getValue());
        Assert.assertEquals(0, invalidSeparatorCounter.getValue());
    }

    @Test
    public void testReducer() throws IOException {
        // Создание входных данных
        Gson gson = new Gson();
        Text id = new Text("b");
        BidDto dto1 = new BidDto(id.toString(), 100, 1);
        BidDto dto2 = new BidDto(id.toString(), 120, 1);
        BidDto dto3 = new BidDto(id.toString(), 240, 1);
        BidDto dto4 = new BidDto(id.toString(), 340, 1);

        List<Text> values = Arrays.asList(new Text(gson.toJson(dto1)), new Text(gson.toJson(dto2)), new Text(gson.toJson(dto3)), new Text(gson.toJson(dto4)));
        // Создание выходных данных
        BidDto answerDto = new BidDto(id.toString(), 340, 4);
        String answerJson = gson.toJson(answerDto);
        // Задание входных параметров reducer
        reduceDriver.withInput(id, values);
        // Задание ожидаемого результата
        reduceDriver.withAllOutput(Collections.singletonList(
                new Pair<>(id, new Text(answerJson))
        ));
        // Запуск теста
        reduceDriver.runTest();
    }

    /**
     * Тест с некорректным значением суммы
     *
     * @throws IOException
     */
    @Test
    public void mapperBadBidTest() throws IOException {
        // Формирование строки
        String strExample = "bcbc973f1a93e22de83133f360759f04\t20131019134022114\t1\tCALAIF9UcIi\tMozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)\t59.34.170.*\t216\t237\t3\t7ed515fe566938ee6cfbb6ebb7ea4995\tea4e49e1a4b0edabd72386ee533de32f\tnull\tALLINONE_F_Width2\t1000\t90\tNa\tNa\t50\t7336\tasd294\t50\tnull\t2259\t10059,14273,10117,10075,10083,10102,10006,10148,11423,10110,10031,10126,13403,10063\n";
        Text text1 = new Text(strExample);
        // Задание входных параметров mapper
        mapDriver.withInput(new IntWritable(1), text1);
        // Запуск теста
        mapDriver.runTest();// проверка счетчиков
        Counter invalidIpCounter = mapDriver.getCounters().findCounter(AnyErrorCounter.ANY_ERROR);
        Counter invalidSeparatorCounter = mapDriver.getCounters().findCounter(BidNoFormatted.BID_NO_FORMATTED_ERROR);
        Assert.assertEquals(0, invalidIpCounter.getValue());
        Assert.assertEquals(1, invalidSeparatorCounter.getValue());
    }

}
